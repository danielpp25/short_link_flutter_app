// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_test/flutter_test.dart';
import 'package:links_shorten/state_management/providers.dart';

import 'package:links_shorten/ui/short_link_app.dart';
import 'package:links_shorten/ui/views/home/home.dart';
import 'package:provider/provider.dart';

void main() {
  testWidgets('Validates title rendered', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MultiProvider(
        providers: Providers.getProviders(), child: const ShortLinkApp()));

    // Verify that our counter starts at 0.
    expect(find.byType(Home), findsOneWidget);
  });
}
