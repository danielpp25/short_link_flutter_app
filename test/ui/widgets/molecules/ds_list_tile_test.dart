import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:links_shorten/ui/widgets/molecules/ds_list_tile.dart';

import '../test_widgets_utils.dart';

void main() {

  testWidgets('Validate DsListTile render', (WidgetTester tester) async {
    const shortLink = 'https://google.com';
    const alias = 'ALIAS: 43876';
    await tester.pumpWidget(TestWidgetsUtils.getApp(const DsListTile(
      shortLink: shortLink,
      alias: alias,
    )));
    final aliasWidget = find.byKey(const Key('ds-list-tile-alias'));
    final linkWidget = find.byKey(const Key('ds-list-tile-link'));

    expect(find.text(alias), findsOneWidget);
    expect(find.text(shortLink), findsOneWidget);
    expect(aliasWidget, findsOneWidget);
    expect(linkWidget, findsOneWidget);


  });
}
