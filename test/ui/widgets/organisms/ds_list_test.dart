import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:links_shorten/domain/short_link/short_link_entity.dart';
import 'package:links_shorten/ui/widgets/organisms/ds_list.dart';

import '../test_widgets_utils.dart';

void main() {

  const aliasLabel = 'ALIAS: ';
  List<ShortLinkEntity> aliasLinks = <ShortLinkEntity>[];

  Future<void> build(WidgetTester tester) async {
    await tester.pumpWidget(
      TestWidgetsUtils.getApp(
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                child: DsList(
              aliasLabel: aliasLabel,
              linksShorten: aliasLinks,
            ))
          ],
        ),
      ),
    );
  }

  testWidgets('Validate DsList render', (WidgetTester tester) async {
    await build(tester);
    expect(find.byType(DsList), findsOneWidget);
    expect(find.text(aliasLabel), findsNothing);

    aliasLinks.add(
      ShortLinkEntity(alias: '43587', links: ['short', 'original']),
    );

    await build(tester);
    expect(find.text('${aliasLabel}43587'), findsOneWidget);
  });
}
