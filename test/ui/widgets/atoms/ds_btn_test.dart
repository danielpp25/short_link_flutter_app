import 'package:flutter_test/flutter_test.dart';
import 'package:links_shorten/ui/widgets/atoms/ds_btn.dart';
import 'package:flutter/material.dart';

import '../test_widgets_utils.dart';

void main() {
  const IconData icon =  Icons.send_outlined;
  bool isPressed = false;
  void _onPressed() {
    isPressed = true;
  }

  testWidgets('Check DsBtn render and tap', (WidgetTester tester) async {

    await tester.pumpWidget(
      TestWidgetsUtils.getApp(
        DsBtnIcon(
          iconData: icon,
          onPressed: _onPressed,
        ),
      ),
    );
    expect(find.widgetWithIcon(DsBtnIcon, icon), findsOneWidget);
    expect(isPressed, false);

    final btn = find.byKey(const Key('ds-btn-icon'));
    await tester.tap(btn);
    expect(isPressed, true);



  });
}
