import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:links_shorten/ui/widgets/atoms/ds_input.dart';

import '../test_widgets_utils.dart';

void main() {
  const String hint = 'Hint Text';
  const String helperText = 'Helper Text';
  String? errorText;
  final TextEditingController controller = TextEditingController();
  bool isTyping = false;

  void onChange(String value) {
    isTyping = true;
    errorText = "Has error";
  }

  Future<void> build(WidgetTester tester) async {
    await tester.pumpWidget(
      TestWidgetsUtils.getApp(
        DsInput(
          hint: hint,
          helperText: helperText,
          onChange: onChange,
          controller: controller,
          errorText: errorText,
        ),
      ),
    );
  }

  testWidgets('Check DsInput render', (WidgetTester tester) async {
    await build(tester);

    expect(find.byKey(const Key('ds-input')), findsOneWidget);
    final input = find.byKey(const Key('ds-input'));
    const text = 'https://adnb.com';
    await tester.enterText(input, text);

    expect(isTyping, true);
    expect(controller.text, text);
    expect(find.text(hint), findsOneWidget);
    expect(find.text(helperText), findsOneWidget);

    await build(tester);

    expect(find.text(errorText!), findsOneWidget);
  });
}
