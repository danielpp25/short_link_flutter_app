import 'package:flutter/material.dart';
import 'package:links_shorten/adapters/common/api_client.dart';
import 'package:links_shorten/state_management/providers.dart';
import 'package:mockito/annotations.dart';
import 'package:provider/provider.dart';

@GenerateMocks([ApiClient])
class TestWidgetsUtils {
  TestWidgetsUtils._();

  static Widget getApp(Widget child) {
    return MaterialApp(
      home: Scaffold(
        body: SizedBox.expand(
          child: Center(
            child: child,
          ),
        ),
      ),
    );
  }

  static Widget getStateManagementApp(Widget child, {ApiClient? apiClient}) {
    return MultiProvider(
        providers: Providers.getProviders(apiClient: apiClient),
        child: getApp(child));
  }

  static Widget getStateMaterialApp(Widget child, {ApiClient? apiClient}) {
    return MultiProvider(
      providers: Providers.getProviders(apiClient: apiClient),
      child: MaterialApp(
        title: 'Shorten Link',
        home: child,
      ),
    );
  }
}
