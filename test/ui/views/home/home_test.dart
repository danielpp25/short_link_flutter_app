import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:links_shorten/adapters/common/api_client.dart';
import 'package:links_shorten/adapters/short_link_adapter/short_link_adapter.dart';
import 'package:links_shorten/ui/views/home/components/home_input_send.dart';
import 'package:links_shorten/ui/views/home/home.dart';
import 'package:links_shorten/ui/widgets/atoms/ds_btn.dart';
import 'package:links_shorten/ui/widgets/organisms/ds_list.dart';
import 'package:mockito/mockito.dart';

import '../../widgets/test_widgets_utils.dart';
import '../../widgets/test_widgets_utils.mocks.dart';

void main() {
  final ApiClient client = MockApiClient();

  Future<void> build(WidgetTester tester) async {
    await tester.pumpWidget(
      TestWidgetsUtils.getStateMaterialApp(const Home(), apiClient: client),
    );
  }

  testWidgets('Validate Home render', (WidgetTester tester) async {
    await build(tester);

    expect(find.byType(HomeInputSend), findsOneWidget);
    expect(find.text('Recently shortened URLs'), findsOneWidget);
    expect(find.byType(DsList), findsOneWidget);
  });

  testWidgets('Validate alias when send a valid URL',
      (WidgetTester tester) async {
    const String url = 'https://localhost';
    const String alias = '43213';

    when(client.post(
      ShortLinkAdapter.kCreateAliasUrl,
      <String, dynamic>{'url': url},
    )).thenAnswer(
      (_) async => ApiModel(
        200,
        {
          'alias': alias,
          '_links': {'short': 'local', 'self': url}
        },
      ),
    );

    await build(tester);

    await tester.enterText(find.byType(TextField), url);
    await tester.pump();

    await tester.tap(find.byType(DsBtnIcon));
    await tester.pump();

    expect(find.text('ALIAS: $alias'), findsOneWidget);
  });
}
