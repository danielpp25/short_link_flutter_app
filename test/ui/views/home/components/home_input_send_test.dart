import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:links_shorten/adapters/common/api_client.dart';
import 'package:links_shorten/adapters/short_link_adapter/short_link_adapter.dart';
import 'package:links_shorten/ui/views/home/components/home_input_send.dart';
import 'package:links_shorten/ui/widgets/atoms/ds_btn.dart';
import 'package:mockito/mockito.dart';

import '../../../widgets/test_widgets_utils.dart';
import '../../../widgets/test_widgets_utils.mocks.dart';


void main() {
  final ApiClient client = MockApiClient();
  bool unFocus = false;
  void _onFocusCalled() {
    unFocus = true;
  }

  Future<void> build(WidgetTester tester) async {
    await tester.pumpWidget(
      TestWidgetsUtils.getStateManagementApp(
        HomeInputSend(
          onFocus: _onFocusCalled,
        ),
        apiClient: client,
      ),
    );
  }

  testWidgets('Validate HomeInputSend render', (WidgetTester tester) async {
    await build(tester);

    expect(find.byType(Row), findsOneWidget);
    expect(find.byType(Expanded), findsOneWidget);
    expect(find.byType(TextField), findsOneWidget);
    expect(find.byType(DsBtnIcon), findsOneWidget);
    expect(find.byIcon(Icons.send_outlined), findsOneWidget);
  });

  testWidgets(
      'Validate if typing an url the widget can send it and display loading icon',
      (WidgetTester tester) async {
    const String url = 'https://localhost';
    const String alias = '43213';

    when(client.post(
      ShortLinkAdapter.kCreateAliasUrl,
      <String, dynamic>{'url': url},
    )).thenAnswer(
      (_) async => Future.delayed(
        const Duration(milliseconds: 100),
        () => ApiModel(
          200,
          {
            'alias': alias,
            '_links': {'short': 'local', 'self': url}
          },
        ),
      ),
    );

    await build(tester);

    await tester.tap(find.byType(DsBtnIcon));
    await tester.pump();
    expect(unFocus, false);

    await tester.enterText(find.byType(TextField), url);
    await tester.pump();

    await tester.tap(find.byType(DsBtnIcon));
    await tester.pump();

    expect(find.byIcon(Icons.refresh), findsOneWidget);
    expect(unFocus, true);

    await tester.pump(const Duration(milliseconds: 200));
  });

  testWidgets(
      'Validate if typing an url and delete it the send button is disabled',
      (WidgetTester tester) async {
    unFocus = false;
    const String url = 'https://localhost';

    await build(tester);

    await tester.enterText(find.byType(TextField), url);
    await tester.pump();

    await tester.enterText(find.byType(TextField), '');
    await tester.pump();

    await tester.tap(find.byType(DsBtnIcon));
    await tester.pump();

    expect(find.byIcon(Icons.refresh), findsNothing);
    expect(unFocus, false);
  });

  testWidgets(
      'Validate if typing an invalid url and send it display error message',
      (WidgetTester tester) async {
    const String invalidUrl = 'asbhasd';
    const errorMessage = 'Please insert a valid URL';

    await build(tester);

    await tester.enterText(find.byType(TextField), invalidUrl);
    await tester.pump();

    await tester.tap(find.byType(DsBtnIcon));
    await tester.pump();

    expect(find.text(errorMessage), findsOneWidget);
  });
}
