import 'package:flutter_test/flutter_test.dart';
import 'package:links_shorten/adapters/common/api_client.dart';
import 'package:links_shorten/adapters/short_link_adapter/short_link_adapter.dart';
import 'package:links_shorten/domain/short_link/short_link_entity.dart';
import 'package:mockito/mockito.dart';

import '../../ui/widgets/test_widgets_utils.mocks.dart';

void main() {

  final client = MockApiClient();
  final shortLinkAdapter = ShortLinkAdapter(client);

  const url = 'https://localhost';

  group('Link shorten', () {
    test('get it successfully', () async {

      when(client.post(
        ShortLinkAdapter.kCreateAliasUrl,
        <String, dynamic>{'url': url},
      )).thenAnswer(
        (_) async => ApiModel(
          200,
          {
            'alias': '43213',
            '_links': {'short': '', 'self': ''}
          },
        ),
      );

      expect(await shortLinkAdapter.createLinkShorten(url),
          isA<ShortLinkEntity>());
    });

    test('failed on get one', () async {

      when(client.post(
        ShortLinkAdapter.kCreateAliasUrl,
        <String, dynamic>{'url': url},
      )).thenAnswer(
        (_) async => ApiModel(
          500,
          {},
        ),
      );

      expect(await shortLinkAdapter.createLinkShorten(url),
          isNull);
    });

    test('failed on get one by client call exception', () async {

      when(client.post(
        ShortLinkAdapter.kCreateAliasUrl,
        <String, dynamic>{'url': url},
      )).thenThrow(Exception());

      expect(await shortLinkAdapter.createLinkShorten(url),
          isNull);
    });
  });

  group('Get original URL by alias', (){
    test('successfully', () async {
      const alias = '43534';

      when(client.get(
        '${ShortLinkAdapter.kCreateAliasUrl}/$alias',
      )).thenAnswer(
            (_) async => ApiModel(
          200,
          {
            'alias': url
          },
        ),
      );

      expect(await shortLinkAdapter.getShortenedUrl(alias), isA<String>());
    });
  });
}
