import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:links_shorten/adapters/common/api_client.dart';
import 'package:links_shorten/adapters/common/environment.dart';
import 'package:links_shorten/adapters/short_link_adapter/short_link_adapter.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'api_client_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  final client = MockClient();

  final queries = <String, dynamic>{};
  String q = Uri(queryParameters: queries).query;

  const String urlToShorten = 'https://localhost';
  final headers = {
    'content-type': 'application/json; charset=utf-8',
  };

  final apiClient = ApiClient(Environment.baseUrl, client);

  group('on GET call', () {
    test('perform API call successfully', () async {
      const String alias = '43213';
      final url = Uri.parse(
          Environment.baseUrl + ShortLinkAdapter.kCreateAliasUrl + '/$alias' + '?' + q);
      when(client.get(url, headers: headers))
          .thenAnswer((_) async => http.Response('{ "url": "43213" }', 200));

      expect(
          await apiClient.get(ShortLinkAdapter.kCreateAliasUrl + '/$alias'), isA<ApiModel>());
    });
  });

  group('on POST call', () {
    test('perform API call successfully', () async {
      final url = Uri.parse(Environment.baseUrl + ShortLinkAdapter.kCreateAliasUrl + '?' + q);
      final data = {'url': urlToShorten};
      headers.addAll({
        'content-length': '${utf8.encode(jsonEncode(data)).length}',
      });
      when(client.post(
        url,
        headers: headers,
        body: jsonEncode(data),
        encoding: Encoding.getByName('utf-8'),
      )).thenAnswer((_) async => http.Response(
          '{ "alias": "43213", "_links": {"short": "", "self": ""} }', 200));

      expect(await apiClient.post(ShortLinkAdapter.kCreateAliasUrl, data), isA<ApiModel>());
    });
  });
}
