import 'package:links_shorten/domain/short_link/short_link_entity.dart';

abstract class ShortLinkGateway {

  Future<ShortLinkEntity?> createLinkShorten(String url);

  Future<String> getShortenedUrl(String alias);
}