class ShortLinkEntity {
  final String? alias;
  final List<String>? links;

  ShortLinkEntity({this.alias, this.links});
}
