import 'dart:convert';

import 'package:http/http.dart' as http;

enum DsApiType {
  get,
  post,
  patch,
  delete,
  put,
}

class ApiModel {
  final int statusCode;
  final Map<String, dynamic> body;

  ApiModel(this.statusCode, this.body);
}

class ApiClient {
  final String? _baseUrl;
  http.Client client;

  ApiClient(this._baseUrl, [client]):
    client = client ?? http.Client();


  Uri getFullUrl(String url, Map<String, dynamic>? queries) {
    String q = Uri(queryParameters: queries).query;
    return Uri.parse(_baseUrl! + url + '?' + q);
  }

  Future<ApiModel> get(String path,
      {Map<String, dynamic>? queries, Map<String, String>? headers}) async {
    final response = await client.get(getFullUrl(path, queries),
        headers: _getHeaders(headers, DsApiType.get));
    return ApiModel(response.statusCode, jsonDecode(response.body));
  }

  Future<ApiModel> post(String path, Map<String, dynamic> data,
      {Map<String, dynamic>? queries, Map<String, String>? headers}) async {
    final response = await client.post(
      getFullUrl(path, queries),
      headers: _getHeaders(headers, DsApiType.post, data: data),
      body: jsonEncode(data),
      encoding: Encoding.getByName('utf-8'),
    );
    return ApiModel(response.statusCode, jsonDecode(response.body));
  }

  Map<String, String> _getHeaders(
      Map<String, String>? additionalHeaders, DsApiType method,
      {Map<String, dynamic>? data}) {
    final headers = {
      'content-type': 'application/json; charset=utf-8',
      ...?additionalHeaders,
    };
    switch (method) {
      case DsApiType.post:
      case DsApiType.patch:
      case DsApiType.put:
        headers.addAll({
          'content-length': '${utf8.encode(jsonEncode(data)).length}',
        });
        break;
      default:
        break;
    }
    return headers;
  }
}
