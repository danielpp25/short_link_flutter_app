import 'package:links_shorten/adapters/common/api_client.dart';
import 'package:links_shorten/domain/short_link/short_link_entity.dart';
import 'package:links_shorten/domain/short_link/short_link_gateway.dart';


class ShortLinkAdapter implements ShortLinkGateway {

  static const kCreateAliasUrl = '/api/alias';

  final ApiClient _client;

  ShortLinkAdapter(this._client);

  @override
  Future<ShortLinkEntity?> createLinkShorten(String url) async {
    try {
      final response =
          await _client.post(kCreateAliasUrl, <String, dynamic>{'url': url});
      if (response.statusCode == 200) {
        final body = response.body;
        return ShortLinkEntity(
            alias: body['alias'],
            links: [body['_links']['short'], body['_links']['self']]);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  @override
  Future<String> getShortenedUrl(String alias) async {
    try {
      final response = await _client.get('$kCreateAliasUrl/$alias');
      if (response.statusCode == 200) {
        return response.body['url'];
      } else {
        return "Not found";
      }
    } catch (e) {
      return e.toString();
    }
  }
}
