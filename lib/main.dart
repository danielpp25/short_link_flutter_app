import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:links_shorten/state_management/providers.dart';
import 'package:links_shorten/ui/short_link_app.dart';

void main() {
  runApp(MultiProvider(
      providers: Providers.getProviders(), child: const ShortLinkApp()));
}
