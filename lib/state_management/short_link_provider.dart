import 'package:flutter/material.dart';
import 'package:links_shorten/domain/short_link/short_link_entity.dart';
import 'package:links_shorten/domain/short_link/short_link_gateway.dart';

class ShortLinkProvider extends ChangeNotifier {
  List<ShortLinkEntity> linksShorten = [];

  final ShortLinkGateway _shortLinkApi;

  ShortLinkProvider(this._shortLinkApi);

  Future<String?> createLinkShorten(String url) async {
    String? message;
    ShortLinkEntity? shortedLink = await _shortLinkApi.createLinkShorten(url);
    message =
        shortedLink != null ? updateList(shortedLink) : "Please try again";
    return message;
  }

  String? updateList(ShortLinkEntity data) {
    linksShorten.add(data);
    notifyListeners();
    return null;
  }
}
