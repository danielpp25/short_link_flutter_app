import 'package:links_shorten/adapters/common/api_client.dart';
import 'package:links_shorten/adapters/common/environment.dart';
import 'package:links_shorten/adapters/short_link_adapter/short_link_adapter.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:links_shorten/state_management/short_link_provider.dart';

class Providers {
  static List<SingleChildWidget> getProviders({ApiClient? apiClient}) {

    final shortLinkApi =
        ShortLinkAdapter(apiClient ?? ApiClient(Environment.baseUrl));

    return <SingleChildWidget>[
      ChangeNotifierProvider<ShortLinkProvider>(
          create: (_) => ShortLinkProvider(shortLinkApi)),
    ];
  }
}
