import 'package:flutter/material.dart';
import 'ds_font_style.dart';
import '../tokens/ds_typography.dart';

class DsText {
  DsText._();

  static _DsTextType small(String label,
      {Key? key,
        TextAlign? align,
        bool? softWrap,
        TextOverflow? overflow,
        TextDecoration? decoration,
        DsFontStyle type = DsFontStyle.small}) =>
      _DsTextType(
        label,
        key: key,
        textAlign: align,
        overflow: overflow,
        softWrap: softWrap,
        decoration: decoration,
        fontSize: DsTypography.smallSize,
        fontFamily: DsTypography.familyRoboto,
        type: type,
      );

  static _DsTextType label(String? label,
      {Key? key,
        TextAlign? align,
        bool? softWrap,
        TextOverflow? overflow,
        TextDecoration? decoration,
        DsFontStyle type = DsFontStyle.label}) =>
      _DsTextType(
        label,
        key: key,
        textAlign: align,
        overflow: overflow,
        softWrap: softWrap,
        decoration: decoration,
        fontSize: DsTypography.labelSize,
        fontFamily: DsTypography.familyRoboto,
        type: type,
      );

  static _DsTextType body(String label,
      {Key? key,
        TextAlign? align,
        bool? softWrap,
        TextOverflow? overflow,
        DsFontStyle type = DsFontStyle.body}) =>
      _DsTextType(
        label,
        key: key,
        textAlign: align,
        overflow: overflow,
        softWrap: softWrap,
        fontSize: DsTypography.bodySize,
        fontFamily: DsTypography.familyRoboto,
        type: type,
      );


  static _DsTextType head(String label,
      {Key? key,
        TextAlign? align,
        bool? softWrap,
        TextOverflow? overflow,
        DsFontStyle type = DsFontStyle.headLine}) =>
      _DsTextType(
        label,
        key: key,
        textAlign: align,
        overflow: overflow,
        softWrap: softWrap,
        fontSize: DsTypography.headSize,
        fontFamily: DsTypography.familyRoboto,
        type: type,
      );

}

class _DsTextType extends StatelessWidget with DsFontStyleMixin {
  final String? label;
  final double? fontSize;
  final String? fontFamily;
  final FontStyle? fontStyle;
  final DsFontStyle? type;
  final bool? softWrap;
  final TextAlign? textAlign;
  final TextOverflow? overflow;
  final TextDecoration? decoration;

  const _DsTextType(
      this.label, {
        Key? key,
        this.type,
        this.textAlign,
        this.overflow,
        this.fontSize,
        this.fontFamily,
        this.fontStyle,
        this.softWrap,
        this.decoration,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      label!,
      softWrap: softWrap,
      style: getStyleBy(
        type,
        fontSize,
        fontStyle: fontStyle,
        decoration: decoration ?? TextDecoration.none,
      ),
      textAlign: textAlign,
      overflow: overflow,
    );
  }
}