import 'package:flutter/material.dart';
import 'package:links_shorten/ui/widgets/tokens/ds_colors.dart';
import 'package:links_shorten/ui/widgets/tokens/ds_typography.dart';

enum DsFontStyle {
  small,
  smallUnSelected,
  smallError,
  label,
  labelUnSelected,
  body,
  headLine,
}

mixin DsFontStyleMixin {
  TextStyle getStyleBy(DsFontStyle? type, double? fontSize,
      {String fontFamily = DsTypography.familyRoboto,
        FontStyle? fontStyle = FontStyle.normal,
        TextDecoration decoration = TextDecoration.none,
        double? height}) {
    TextStyle style;
    Color color;
    FontWeight weight;

    switch (type) {
      case DsFontStyle.labelUnSelected:
      case DsFontStyle.smallUnSelected:
        color = DsColors.textUnselected;
        weight = FontWeight.w500;
        break;
      case DsFontStyle.smallError:
        color = DsColors.error;
        weight = FontWeight.w500;
        break;
      case DsFontStyle.body:
        color = DsColors.text;
        weight = FontWeight.w600;
        break;
      case DsFontStyle.headLine:
        color = DsColors.textHeadline;
        weight = FontWeight.w800;
        break;
      case DsFontStyle.small:
      case DsFontStyle.label:
      default:
        color = DsColors.text;
        weight = FontWeight.w500;
        break;
    }
    style = TextStyle(
        decoration: decoration,
        fontSize: fontSize,
        fontWeight: weight,
        fontFamily: fontFamily,
        fontStyle: fontStyle,
        color: color,
        height: height,
        package: 'design');
    return style;
  }
}