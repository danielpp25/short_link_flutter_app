import 'package:flutter/material.dart';
import 'package:links_shorten/domain/short_link/short_link_entity.dart';
import 'package:links_shorten/ui/widgets/molecules/ds_list_tile.dart';
import 'package:links_shorten/ui/widgets/tokens/ds_colors.dart';

class DsList extends StatelessWidget {
  final List<ShortLinkEntity>? linksShorten;
  final String? aliasLabel;

  const DsList({Key? key, this.linksShorten, this.aliasLabel = 'ALIAS: '})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      key: const Key('ds-list'),
      shrinkWrap: true,
      itemCount: linksShorten?.length ?? 0,
      itemBuilder: (context, index) {
        return DsListTile(
            key: Key('ds-list-item-$index'),
            alias: '$aliasLabel${linksShorten?.elementAt(index).alias}',
            shortLink: linksShorten![index].links?.last);
      },
      separatorBuilder: (context, index) => const Divider(
        color: DsColors.line,
      ),
    );
  }
}
