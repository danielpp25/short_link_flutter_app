import 'package:flutter/material.dart';
import 'package:links_shorten/ui/widgets/foundation/ds_text.dart';

class DsListTile extends StatelessWidget {
  final String? alias;
  final String? shortLink;

  const DsListTile({Key? key, this.alias, this.shortLink}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      key: const Key('ds-list-tile'),
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          DsText.body('$shortLink', key: const Key('ds-list-tile-link')),
          const SizedBox(height: 10),
          DsText.label('$alias', key: const Key('ds-list-tile-alias')),
        ],
      ),
    );
  }
}
