import 'package:flutter/material.dart';
import 'package:links_shorten/ui/widgets/tokens/ds_colors.dart';

const _kRadius = 20.0;

class DsBtnIcon extends StatelessWidget {
  final IconData? iconData;
  final VoidCallback? onPressed;

  const DsBtnIcon({Key? key, this.iconData, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        key: const Key('ds-btn-icon'),
        height: 35.0,
        width: 55.0,
        child: ElevatedButton(
          onPressed: onPressed,
          child: Icon(
            iconData,
            color: onPressed != null
                ? DsColors.icon
                : DsColors.icon.withOpacity(0.35),
            size: 20,
          ),
          style: ButtonStyle(
            padding: MaterialStateProperty.all(const EdgeInsets.only(left: 0)),
            backgroundColor: MaterialStateProperty.all(DsColors.primary),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(_kRadius),
                side: BorderSide(
                    color: DsColors.text.withOpacity(0.2), width: 0.0),
              ),
            ),
          ),
        ));
  }
}
