import 'package:flutter/material.dart';
import 'package:links_shorten/ui/widgets/foundation/ds_font_style.dart';
import 'package:links_shorten/ui/widgets/tokens/ds_colors.dart';
import 'package:links_shorten/ui/widgets/tokens/ds_typography.dart';

const _kRadius = 20.0;

class DsInput extends StatelessWidget with DsFontStyleMixin {
  final String? hint;
  final String? helperText;
  final String? errorText;
  final TextEditingController? controller;
  final ValueChanged<String>? onChange;

  const DsInput({
    Key? key,
    this.hint,
    this.helperText,
    this.controller,
    this.errorText,
    this.onChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      key: const Key('ds-input'),
      height: 60,
      padding:
          EdgeInsets.only(top: 12.0, bottom: errorText == null ? 12.0 : 0.0),
      child: TextField(
        key: key,
        controller: controller,
        onChanged: onChange,
        decoration: InputDecoration(
          hintText: '$hint',
          hintStyle: getStyleBy(
            DsFontStyle.labelUnSelected,
            DsTypography.labelSize,
          ),
          helperText: helperText,
          helperStyle: getStyleBy(DsFontStyle.small, DsTypography.smallSize,
              height: 0.4),
          errorText: errorText,
          errorStyle: getStyleBy(
            DsFontStyle.smallError,
            DsTypography.smallSize,
            height: 0.4,
          ),
          filled: true,
          fillColor: DsColors.primary,
          contentPadding: const EdgeInsets.only(left: 15.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(Radius.circular(_kRadius)),
            borderSide:
                BorderSide(color: DsColors.text.withOpacity(0.1), width: 0.0),
          ),
          errorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(_kRadius)),
            borderSide: BorderSide(color: Colors.redAccent, width: 1.0),
          ),
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(Radius.circular(_kRadius)),
            borderSide:
                BorderSide(color: DsColors.text.withOpacity(0.1), width: 1.0),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(_kRadius)),
              borderSide: BorderSide(
                  color: DsColors.text.withOpacity(0.5), width: 1.5)),
          prefixIconConstraints:
              const BoxConstraints(minWidth: (25), maxWidth: (50)),
          suffixIconConstraints:
              const BoxConstraints(minWidth: (25), maxWidth: (50)),
        ),
      ),
    );
  }
}
