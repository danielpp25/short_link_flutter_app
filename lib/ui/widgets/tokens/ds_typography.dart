class DsTypography {
  DsTypography._();
  //fonts

  static const String familyRoboto = 'Roboto';

  static const double smallSize = 10.0;
  static const double labelSize = 14.0;
  static const double bodySize = 16.0;
  static const double headSize = 18.0;


}