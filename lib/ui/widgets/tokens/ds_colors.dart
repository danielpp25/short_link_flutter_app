
import 'package:flutter/material.dart';

class DsColors {
  DsColors._();

  static const primary = Color(0xFFE9E9E9);
  static const background = Color(0xFFFFFFFF);
  static const text = Color(0xFF5E5E5E);
  static const textUnselected = Color(0xA05E5E5E);
  static const textHeadline = Color(0xFF202020);

  static const icon = Color(0xFF202020);
  static const line = Color(0xFF6F6F6F);
  static const error = Color(0xFFE53232);
}