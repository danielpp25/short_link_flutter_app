abstract class HomeInputSendInterface {
  void showLoading();
  void hideLoading();
  void onUrlShortenedSuccessfully();
  void onUrlShortenedFailed(String message);
}