import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:links_shorten/state_management/short_link_provider.dart';
import 'package:links_shorten/ui/views/home/components/home_input_send_interface.dart';
import 'package:links_shorten/ui/views/home/components/home_input_send_presenter.dart';
import 'package:links_shorten/ui/widgets/atoms/ds_btn.dart';
import 'package:links_shorten/ui/widgets/atoms/ds_input.dart';

class HomeInputSend extends StatefulWidget {
  final VoidCallback? onFocus;

  const HomeInputSend({Key? key, this.onFocus}) : super(key: key);

  @override
  _HomeInputSendState createState() => _HomeInputSendState();
}

class _HomeInputSendState extends State<HomeInputSend>
    implements HomeInputSendInterface {
  final _urlController = TextEditingController();
  bool _canSend = false;
  String? _errorText;
  IconData _btnIcon = Icons.send_outlined;
  late HomeInputSendPresenter _presenter;

  @override
  void initState() {
    super.initState();
    _presenter = HomeInputSendPresenter(
        this, Provider.of<ShortLinkProvider>(context, listen: false));
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: DsInput(
            key: const Key('home-input-send-text'),
            hint: 'https://domain.com',
            errorText: _errorText,
            controller: _urlController,
            onChange: _onInputChange,
          ),
        ),
        const SizedBox(
          width: 10.0,
        ),
        DsBtnIcon(
          key: const Key('home-input-send-btn'),
          iconData: _btnIcon,
          onPressed:
              _canSend ? () => _onSendBtnPressed(_urlController.text) : null,
        ),
      ],
    );
  }

  void _onSendBtnPressed(String url) {
    _canSend = false;
    widget.onFocus?.call();
    _presenter.sendUrlToShortIt(url);
    setState(() {});
  }

  void _onInputChange(String value) {
    _errorText = null;
    if (value.isNotEmpty && !_canSend) {
      _canSend = true;
      setState(() {});
    } else if (value.isEmpty && _canSend) {
      _canSend = false;
      setState(() {});
    }
  }

  @override
  void onUrlShortenedFailed(String message) {
    _errorText = message;
    _canSend = true;
    setState(() {});
  }

  @override
  void onUrlShortenedSuccessfully() {
    _errorText = null;
    _canSend = true;
    setState(() {});
  }

  @override
  void hideLoading() {
    setState(() {
      _btnIcon = Icons.send_outlined;
    });
  }

  @override
  void showLoading() {
    setState(() {
      _btnIcon = Icons.refresh;
    });
  }
}
