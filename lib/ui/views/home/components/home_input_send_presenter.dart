import 'package:links_shorten/state_management/short_link_provider.dart';
import 'package:links_shorten/ui/views/home/components/home_input_send_interface.dart';

class HomeInputSendPresenter {
  final HomeInputSendInterface _interface;
  final ShortLinkProvider _provider;

  HomeInputSendPresenter(
    this._interface,
    this._provider,
  );

  Future<void> sendUrlToShortIt(String url) async {
    String? message;
    final bool validURL = Uri.parse(url).isAbsolute;
    if (validURL) {
      _interface.showLoading();
      message = await _provider.createLinkShorten(url);
      _interface.hideLoading();
    } else {
      message = 'Please insert a valid URL';
    }
    _handleResponse(message);
  }

  void _handleResponse(String? message) {
    if (message == null) {
      _interface.onUrlShortenedSuccessfully();
    } else {
      _interface.onUrlShortenedFailed(message);
    }
  }
}
