import 'package:flutter/material.dart';
import 'package:links_shorten/ui/widgets/organisms/ds_list.dart';
import 'package:provider/provider.dart';
import 'package:links_shorten/state_management/short_link_provider.dart';
import 'package:links_shorten/ui/views/home/components/home_input_send.dart';
import 'package:links_shorten/ui/widgets/foundation/ds_text.dart';
import 'package:links_shorten/ui/widgets/tokens/ds_colors.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapUp: (_) => _onFocus(context),
      child: Scaffold(
        appBar: AppBar(toolbarHeight: 0, backgroundColor: DsColors.primary),
        body: Container(
          key: const Key('home'),
          margin: const EdgeInsets.only(top: 55.0),
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              HomeInputSend(
                key: const Key('Home-input-send'),
                onFocus: () => _onFocus(context),
              ),
              const SizedBox(height: 45),
              DsText.head('Recently shortened URLs'),
              const SizedBox(height: 35),
              Expanded(
                child: Consumer<ShortLinkProvider>(
                  builder: (context, data, child) {
                    return DsList(
                      key: const Key('list-alias'),
                      linksShorten: data.linksShorten,
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onFocus(BuildContext context) {
    FocusScope.of(context).unfocus();
  }
}
