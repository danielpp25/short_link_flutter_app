import 'package:flutter/material.dart';
import 'package:links_shorten/ui/views/home/home.dart';

class ShortLinkApp extends StatelessWidget {

  const ShortLinkApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shorten Link',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Home(),
    );
  }
}